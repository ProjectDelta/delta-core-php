<?php

namespace Delta {

	use PDO;
	use PDOException;
	use PDOStatement;
	
	class Database {

		private ?PDO $connection = null;

		private string $host;
		private int $port;
		private string $name;
		private string $user;
		private string $pass;
		private array $options;


		public function __construct(string $host, int $port, string $user, string $pass, string $name, array $options = []) {
			$this->host = $host;
			$this->port = $port;
			$this->user = $user;
			$this->pass = $pass;
			$this->name = $name;
			$this->options = $options;
			//$this->connection = new PDO($this->getDSN(), $this->user, $this->pass);
		}

		public function __destruct() {
			$this->connection = null;
		}

		public function devError() {
			$result = "PDO::error";
			$result .= "\nPDO::errorCode(): ".$this->connection->errorCode()."";
			$result .= "\nPDO::errorInfo():\n";
			$result .= print_r($this->connection->errorInfo(), true);
			return $result;
		}

		public function ping(bool $throwException = false) : bool {
			try {
				$result = $this->execute("SELECT 1;");
				if($result == 1) {
					return true;
				}
			}
			catch(PDOException $ex) {
				if($throwException) {
					throw $ex;
				}
				//Something todo here?
			}
			return false;
		}


		public function quote(string $string, int $type = PDO::PARAM_STR) : string | false {
			$this->connect();
			return $this->connection->quote($string, $type);
		}

		public function query(string $sql, array $params = []) : PDOStatement | false {
			$stmt = $this->prepare($sql);
			if($stmt->execute($params)) {
				return $stmt;
			}
			return false;
		}
			
		public function prepare(string $sql) : PDOStatement | false {
			$this->connect();
			return $this->connection->prepare($sql);
		}
			
		public function execute(string $sql, array $params = []) : int | false {
			$stmt = $this->prepare($sql);
			if($stmt->execute($params) !== false) {
				return $stmt->rowCount();
			}
			return false;
		}

		public function fetch(string $sql, array $params = [], int $mode = PDO::FETCH_ASSOC) : mixed {
			$stmt = $this->prepare($sql);
			if ($stmt->execute($params) !== false) {
				return $stmt->fetch($mode);
			}
			return false;
		}
		
		//TODO return type
		public function fetchColumn(string $sql, array $params = [], int $column = 0) : mixed {
			$stmt = $this->prepare($sql);
			if($stmt->execute($params) !== false) {
				return $stmt->fetchColumn($column);
			}
			return false;
		}

		public function fetchObject(string $sql, array $params = [], string $class = "stdClass", array $constructorArgs = []) : object | false {
			$stmt = $this->prepare($sql);
			if ($stmt->execute($params) !== false) {
				return $stmt->fetchObject($class, $constructorArgs);
			}
			return false;
		}

		public function fetchAllObject(string $sql, array $params = [], string $class = "stdClass", array $constructorArgs = []) : array | false {
			$stmt = $this->prepare($sql);
			if ($stmt->execute($params) !== false) {
				$result = [];
				while ($row = $stmt->fetchObject($class, $constructorArgs)) {
					$result[] = $row;
				}
				return $result;
			}
			return false;
		}

		public function fetchAll(string $sql, array $params = [], int $mode = PDO::FETCH_DEFAULT) : array | false {
			$stmt = $this->prepare($sql);
			if ($stmt->execute($params) !== false) {
				return $stmt->fetchAll($mode);
			}
			return false;
		}

		public function isTableExists(string $table) : bool {
			$result = $this->fetch("SELECT count(*) as count FROM information_schema.tables WHERE table_schema = :schema AND table_name = :table ", [
		        	"schema" => $this->name,
		        	"table" => $table
			    ]);
				return (intval($result['count']) !== 0);
			}

		public function insert(string $sql, array $params = []) : int | false {
			if($this->execute($sql, $params)) {
				return $this->connection->lastInsertId();
			}
			return false;
		}


		//TODO: Test Transactions
		public function beginTransaction() : bool {
			return $this->connection->beginTransaction();
		}

		public function rollback() : bool {
			if($this->connection->inTransaction())
				return $this->connection->rollback();
			return false;
		}

		public function commit() : bool {
			if($this->connection->inTransaction())
				return $this->connection->commit();
			return false;
		}


		private function connect() {
		    if ($this->connection == null) {
	            $this->connection = new PDO($this->getDSN(), $this->user, $this->pass, $this->options);
		    }
		}

		private function getDSN() : string {
			return "mysql:host=".$this->host.";port=".$this->port.";dbname=".$this->name.";";
		}

	}

}

?>
