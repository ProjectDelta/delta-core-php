<?php

	namespace Delta {

		use Delta\Exceptions\DuplicateServiceException;
		use Delta\Exceptions\UnknownServiceException;

		class Core {

			/**
			 * The the base Path of app
			 *
			 * @type string
			 */
			protected string $path;

			protected array $services = [];

			public function __construct(string $path) {
				$this->path = $path;
			}

			public function __destruct() {
				foreach($this->services as $name=>$service) {
					$this->services[$name] = null;
				}
			}

			public function getPath(string $path = "") : string {
				return $this->path . $path;
			}


			/**
			 * Register a lazy service
			 *
			 * @param string $name                  The name of the service
			 * @param callable $closure             The callable function to execute when requesting our service
			 * @throws DuplicateServiceException    If an attempt is made to register two services with the same name
			 * @return mixed
			 */
			public function register($name, $closure) : void {
				if (isset($this->services[$name])) {
				    throw new DuplicateServiceException('A service is already registered under '. $name);
				}

				$this->services[$name] = function () use ($closure) {
				    static $instance;
				    if (null === $instance) {
				        $instance = $closure();
				    }

				    return $instance;
				};
			}

			/**
			 * Magic "__get" method
			 *
			 * Allows the ability to arbitrarily request a service from this instance
			 * while treating it as an instance property
			 *
			 * This checks the lazy service register and automatically calls the registered
			 * service method
			 *
			 * @param string $name              The name of the service
			 * @throws UnknownServiceException  If a non-registered service is attempted to fetched
			 * @return mixed
			 */
			public function __get(string $name) : mixed {
				if (!isset($this->services[$name])) {
				    throw new UnknownServiceException('Unknown service '. $name);
				}

				if(is_callable($this->services[$name])) {
					$this->services[$name] = $this->services[$name]();
				}

				return $this->services[$name];
			}

		}

	}

?>
